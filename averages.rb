# Extend a String class so it will now have is_number? method to check whether
# the current object is number.
class String
  def is_number?
    true if Integer(self) rescue false
  end
end


# Creates an N-length array with random integers from 1 to 9 where N is a
# value inputed by user from keyboard
def generate_array_from_user_input
  array_length = ''

  while !array_length.is_number?
    print 'Please, enter the desired array length: '
    array_length = gets.chomp
  end

  return Array.new(array_length.to_i) { rand(1...9) }
end


def calculate_mean_average(array)
  return array.sum(0.0) / array.length
end


def calculate_geometric_mean(array)
  sum = array.inject(0) { |memo, v| memo + Math.log(v) }
  sum /= array.size
  return Math.exp(sum).round(2)
end


arr = generate_array_from_user_input
puts 'Calculate mean values for array: ' + arr.join(', ')
puts 'Mean average: ' + calculate_mean_average(arr).to_s
puts 'Geometric mean: ' + calculate_geometric_mean(arr).to_s
