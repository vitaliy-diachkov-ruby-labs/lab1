# This class selects positive even numbers from given array
class ArrayItemsSelector
  def initialize(array)
    @array = array
  end

  def select
    @array.select {|number| number.even? and number > 0}
  end
end

array = Array.new(10) {rand(-9...9)}
puts 'Selecting postitve even numbers from array [' + array.join(', ') + ']'

selector = ArrayItemsSelector.new(array)
puts 'Selected numbers: ' + selector.select.join(', ')
