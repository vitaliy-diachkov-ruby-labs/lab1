# Calculates product of given array, assuming that array contains only
# integers or float values
def product(arr)
  # By default, the last function statement is returned
  arr.inject {|product, n| product * n}
  # equal to: return arr.inject {|product, n| product * n}
end

# Extend a String class so it will now have is_number? method to check whether
# the current object is number.
class String
  def is_number?
    true if Integer(self) rescue false
  end
end


# Gets the desired array length from user. Checks if user input is a number
# So the array could be generated from this value
array_length = ''
while !array_length.is_number?
  # `print` works exactly like `puts`, but it doesn't put \n character at the
  # end of line
  print 'Please, enter the desired array length: '

  # .chomp removes \n character from the end of user input
  array_length = gets.chomp
end

# Calculates product for given array, shows result as program output
arr = Array.new(array_length.to_i) { rand(1...9) }
puts 'Calculating product for array: ' + arr.join(', ')
puts 'Product = ' + product(arr).to_s
