# This program generates 10 random numbers from -9 to 9 and selects only those
# of them that are less then 0 and could be divided by 3 without any leftover

class ArrayItemsSelector
  def initialize(array)
    @array = array
  end

  def select
    @array.select {|number| number < 0 and number % 3 == 0}
  end
end

array = Array.new(10) {rand(-9...9)}
puts 'Selecting numbers from array [' + array.join(', ') + '] that are less ' \
  'then zero and can be divided by 3 without leftover'

selector = ArrayItemsSelector.new(array)
puts 'Selected numbers: ' + selector.select.join(', ')
