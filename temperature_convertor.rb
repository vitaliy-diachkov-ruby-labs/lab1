# This class converts celcius value of temperature to farenheit and kelvin
class TemperatureConvertor
  def initialize(celsius_value)
    @celsius_value = celsius_value
  end

  # Converts celsius to farenheit
  def to_farenheit
    @celsius_value * 1.8 + 32
  end

  # Converts celsius to kelvin
  def to_kelvin
    @celsius_value + 273.15
  end
end


celsius_temperatures = Array.new(10) {rand(16...24)}

celsius_temperatures.each do |celsius_temperature|
  convertor = TemperatureConvertor.new(celsius_temperature)
  puts 'C: %{celsius} | F: %{farenheit} | K: %{kelvin}' % {
    'celsius': celsius_temperature,
    'farenheit': convertor.to_farenheit,
    'kelvin': convertor.to_kelvin
  }
end
